package com.ohgiraffers.common;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

/* Connection을 사용하는 메소드들에서
 * 공통적으로 실행해야 되는 부분을 추출해서
 * 따로 만들어둔 메소드들을 모아둔 클래스
 * */
public class JDBCTemplate {

	/* getConnection() 
	 * config폴더 안에있는 driver.properties를 연결,
	 * 그 안에 들어있는 경로를 의미하는 문자열들을 불러와 사용한다.
	 * Class.forname(driver) , con = DriverManager.getConnection(url, prop) 이부분에서
	 * oracle과 연결을 한다.
	 * con.setAutoCommit(false); 이부분은 자동으로 커밋되는걸 방지하기 위해 기본값을 커밋 안하는 false로 설정했다.
	 * DB와 연결이 완료된 Connection 타입의 변수 con을 리턴해준다.
	 * */
	public static Connection getConnection() {
		
		Connection con = null;
		
		Properties prop = new Properties();
		
		try {
			prop.load(new FileReader("config/driver.properties"));
			
			String driver = prop.getProperty("driver");
			String url = prop.getProperty("url");
			
			Class.forName(driver);
			con = DriverManager.getConnection(url, prop);
			
			con.setAutoCommit(false);
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return con;
	}
	
	/* 메모리 누수를 막기위해 Connection의 할당을 해제하는 부분이다
	 * con이 null이면 인스턴스가 없다는 의미인데 con.close()를 하면
	 * Null Pointer Exception이 발생한다.
	 * 이미 닫혀있다면 다시 닫아줄 필요가 없다.*/
	public static void close(Connection con) {
		
		try {
			if(con != null && !con.isClosed()){
				con.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/* DB에서 조회된 값들을 Java에서 사용할때,
	 * 임시로 값들을 저장해 Java쪽으로 옮기게 해주는 ResultSet의
	 * 메모리 할당을 해제해주는 메소드 
	 * rset이 null일때나, 이미 닫혀있을때는 닫지 않는다.*/
	public static void close(ResultSet rset) {
		
		try {
			if(rset != null && !rset.isClosed()) {
				rset.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/* DB와 연결이 됐을때, 자바에서 작성한 쿼리문을 
	 * DB에서 실행시켜주는 역할을 하는 Statement의 메모리 할당을
	 * 해제해주는 부분.
	 * 마찬가지로 null이거나 , 이미 메모리할당이 해제되었을 떄는 
	 * close를 하지 않는다.
	 * */
	
	public static void close(Statement stmt) {
		
		try {
			if(stmt != null && !stmt.isClosed()) {
				stmt.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	/* 쿼리문을 수행하고, 그 결과를 DB에 저장하는 부분
	 * Connection 변수가 null이거나 이미 해제가 됐을때는
	 * close()를 하지 않는다 */
	public static void commit(Connection con) {
		try {
			if(con != null && !con.isClosed()) {
				con.commit();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/* 쿼리문을 수행했지만, 그 결과를 DB에 저장하지 않고, 원래대로 
	 * 되돌리고 싶을때 사용하는 메소드(RollBack)
	 * con의 값이 null이거나 이미 close된 상태라면
	 * close()를 하지 않는다*/
	public static void rollback(Connection con) {
		try {
			if(con != null && !con.isClosed()) {
				con.rollback();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}