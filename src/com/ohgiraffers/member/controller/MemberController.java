package com.ohgiraffers.member.controller;

import java.util.List;
import java.util.Map;

import com.ohgiraffers.member.model.dto.MemberDTO;
import com.ohgiraffers.member.model.service.MemberService;
import com.ohgiraffers.member.views.MemberResultView;

public class MemberController {

	
	
	private MemberResultView memberResultVIew = new MemberResultView();
	private MemberService memberService = new MemberService();
	
	/* 회원가입하는 기능을 한다.
	 * 회원들의 개인정보를 Map<String, String>형태로 컬럼명과 컬럼값 형태로  전달받는다
	 * 전달받은 회원정보를 MemberDTO에 저장해, 서비스에있는 회원가입 메소드로 전달하고, 
	 * 그 결과를 반환받는다 */
	public void registNewMember(Map<String, String> inputMember) {
		MemberDTO member = new MemberDTO();
		member.setMemberId(inputMember.get("id"));
		member.setMemberPwd(inputMember.get("pwd"));
		member.setMemberName(inputMember.get("name"));
		member.setGender(inputMember.get("gender"));
		member.setEmail(inputMember.get("email"));
		member.setPhone(inputMember.get("phone"));
		member.setAddress(inputMember.get("address"));
		member.setAge(Integer.valueOf(inputMember.get("age")));
		
		int registResult = memberService.insertMember(member);
		
		if(registResult > 0) {
			memberResultVIew.displayDmlResult("insertSuccess");
		} else {
			memberResultVIew.displayDmlResult("insertFailed");
		}
		
	}

	/* 서비스에 있는 모든 회원정보를 반환하는 메소드를 호출한 후
	 * 반환받은 회원정보의 목록을 저장한 후 , 뷰에있는 메소드를 호출한다. */
	public void selectAllMembers() {
		
		List<MemberDTO> memberList = memberService.selectAllMembers();
		
		if(!memberList.isEmpty()) {
			memberResultVIew.display(memberList);
		} else {
			memberResultVIew.displayDmlResult("selectFailed");
		}
		
	}

	/* 아이디를 서비스로 보낸 후 , 반환받은 회원정보를 뷰로 전달하는 기능을 하는 메소드 */
	public void searchMemberById(String id) {
		
		MemberDTO member = memberService.selectById(id);
		
		if(member != null) {
			memberResultVIew.display(member);
		} else {
			memberResultVIew.displayDmlResult("selectFailed");
		}
		
	}

	/* 특정 성별을 서비스에있는 메소드로 전달해 , 그 성별에 해당하는 회원들의 목록을 받은 후
	 * 출력을 담당하는 뷰로 전달하는 메소드 */
	public void searchMemberByGender(String gender) {
		List<MemberDTO> memberList = memberService.selectByGender(gender);
		
		if(!memberList.isEmpty()) {
			memberResultVIew.display(memberList);
		} else {
			memberResultVIew.displayDmlResult("selectFailed");
		}
		
	}

	/* 특정 회원의 비밀번호를 변경하기 위해 , 그 회원의 아이디와 변경할 비밀번호를 
	 * 해당기능의 서비스 메소드에 전달하여 호출한 후, 수행결과를 반환받으면 
	 * 해당 결과에 맞는 뷰를 호출해주는 메소드 */
	public void modifyPassword(String memberId, String password) {
		int modifyResult = memberService.updatePassword(memberId, password);
		
		if(modifyResult > 0) {
			memberResultVIew.displayDmlResult("updateSuccess");
		} else {
			memberResultVIew.displayDmlResult("updatefailed");
		}
		
	}

	/* 특정 회원의 이메일를 변경하기 위해 , 그 회원의 아이디와 변경할 이메일을 
	 * 해당기능의 서비스 메소드에 전달하여 호출한 후, 수행결과를 반환받으면 
	 * 해당 결과에 맞는 뷰를 호출해주는 메소드 */
	
	public void modifyEmail(String memberId, String email) {
		int modifyResult = memberService.updateEmail(memberId, email);
		
		if(modifyResult > 0) {
			memberResultVIew.displayDmlResult("updateSuccess");
		} else {
			memberResultVIew.displayDmlResult("updatefailed");
		}
		
	}

	/* 특정 회원의 핸드폰번호를 변경하기 위해 , 그 회원의 아이디와 변경할 전화번호를 
	 * 해당기능의 서비스 메소드에 전달하여 호출한 후, 수행결과를 반환받으면 
	 * 해당 결과에 맞는 뷰를 호출해주는 메소드 */
	
	public void modifyPhone(String memberId, String phone) {

		int modifyResult = memberService.updatePhone(memberId, phone);
		
		if(modifyResult > 0) {
			memberResultVIew.displayDmlResult("updateSuccess");
		} else {
			memberResultVIew.displayDmlResult("updatefailed");
		}
	}

	/* 특정회원의 아이디와 변경할 이메일을 
	 * 서비스에있는 메소드로 전달해 호출한다.
	 * 그 메소드의 수행결과가 int형으로 반환이 되고, 
	 * 그 결과에 맞는 뷰를 호출해준다 */
	public void modifyAddress(String memberId, String address) {
		int modifyResult = memberService.updateAddress(memberId, address);
		
		if(modifyResult > 0) {
			memberResultVIew.displayDmlResult("updateSuccess");
		} else {
			memberResultVIew.displayDmlResult("updatefailed");
		}
	}

	/* 회원정보를 삭제하는 기능을 하는 서비스의 메소드에 특정 회원의 아이디를 전달해서 호출하면
	 * 해당 결과를 반환받는다. 그 결과에 따라 , 맞는 뷰를 호출한다 */
	public void deleteMember(String memberId) {
		int deleteResult = memberService.deleteMember(memberId);
		
		if(deleteResult > 0) {
			memberResultVIew.displayDmlResult("deleteSuccess");
		} else {
			memberResultVIew.displayDmlResult("deletefailed");
		}
	}

}
