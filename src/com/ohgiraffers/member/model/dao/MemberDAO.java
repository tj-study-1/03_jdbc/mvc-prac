package com.ohgiraffers.member.model.dao;

import static com.ohgiraffers.common.JDBCTemplate.close;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


import com.ohgiraffers.member.model.dto.MemberDTO;

/* DB에 쿼리문을 사용해서 DB에 변화가 생기거나, DB에서 값을 조회하는 기능들을
 * 하는 메소드들의 모음*/
public class MemberDAO {

	private Properties prop = new Properties();
	/* 여러개의 쿼리문을 mapper/member-query.xml 이 경로에 저장해 두고
	 * 거기에 있는 쿼리문들을 Properties를 사용해, 저장된 쿼리문의 이름을 key값으로 주고
	 * 반환받아 사용해야되는데,
	 * MemberDAO를 생성할 때 , 기본적으로 Properties 타입의 변수 prop에 
	 * 연결(연동)을 해놓아서, MemberDAO안의 여러 메소드들이 각각 mapper/member-query.xml에
	 * 접근하지 않게 불필요한 코드를 줄였다. */
	public MemberDAO() {
		
		try {
			prop.loadFromXML(new FileInputStream("mapper/member-query.xml"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	/* 새로운 회원을 회원테이블에 insert하는 메소드
	 * 회원의 모든 정보를 저장하는 자료형인 MemberDTO를 사용해
	 * 각각의 필드들을 getter로 값을 불러와 PreparedStatement를 사용해
	 * 하나한 insert해준다.
	 * PreparedStatement안에 있는 메소드인 executeUpdate()를 호출해서 
	 * 그 값을 int자료형인 result에 넣어 return을 한다.
	 * executeUpdate()는 pstmt로 쿼리문을 DB에서 수행했을때, 오류없이 잘 수행됐다면
	 * 1, 만약 기능동작중 문제가 생겼다면 0을 반환한다.
	 * */
	public int insertMember(Connection con, MemberDTO member) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertMember");
				
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, member.getMemberId());
			pstmt.setString(2, member.getMemberPwd());
			pstmt.setString(3, member.getMemberName());
			pstmt.setString(4, member.getGender());
			pstmt.setString(5, member.getEmail());
			pstmt.setString(6, member.getPhone());
			pstmt.setString(7, member.getAddress());
			pstmt.setInt(8, member.getAge());
			
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}
	
	/* 모든 회원의 정보를 List<MemberDTO>형태로 저장해서 반환하는메소드
	 * TBL_MEMBER의 모든 정보를 조회하는 쿼리문을 사용해 그 결과를
	 * ResultSet에 담은 후 , List<MemberDTO>에 MemberDTO형태로 하나하나씩 저장한다
	 * ResultSet의 처음부터 마지막까지 저장이 끝나면 
	 * List<MemberDTO> 를 반환한다. 
	 * */
	public List<MemberDTO> selectAllMembers(Connection con){
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		List<MemberDTO> memberList = null;
		
		String query = prop.getProperty("selectAllMembers");
		
		try {
			pstmt = con.prepareStatement(query);
			
			rset = pstmt.executeQuery();
			
			memberList = new ArrayList<>();
			while(rset.next()) {
				MemberDTO member = new MemberDTO();
				member.setMemberNo(rset.getInt("MEMBER_NO"));
				member.setMemberId(rset.getString("MEMBER_ID"));
				member.setMemberPwd(rset.getString("MEMBER_PWD"));
				member.setMemberName(rset.getString("MEMBER_NAME"));
				member.setGender(rset.getString("GENDER"));
				member.setEmail(rset.getString("EMAIL"));
				member.setPhone(rset.getString("PHONE"));
				member.setAddress(rset.getString("ADDRESS"));
				member.setAge(rset.getInt("AGE"));
				member.setEnrollDate(rset.getDate("ENROLL_DATE"));
				
				memberList.add(member);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		
		return memberList;
	}
	
	/* parameter로 받은 id값을 가진 회원의 정보를 MemberDTO 자료형 형태로 리턴해주는 메소드
	 * */ 
	public MemberDTO selectById(Connection con, String id) {
		
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		MemberDTO member = null;
		
		String query = prop.getProperty("selectById");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, id);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				member = new MemberDTO();
				
				member.setMemberNo(rset.getInt("MEMBER_NO"));
				member.setMemberId(rset.getString("MEMBER_ID"));
				member.setMemberPwd(rset.getString("MEMBER_PWD"));
				member.setMemberName(rset.getString("MEMBER_NAME"));
				member.setGender(rset.getString("GENDER"));
				member.setEmail(rset.getString("EMAIL"));
				member.setPhone(rset.getString("PHONE"));
				member.setAddress(rset.getString("ADDRESS"));
				member.setAge(rset.getInt("AGE"));
				member.setEnrollDate(rset.getDate("ENROLL_DATE"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		return member;
	}

	/* 비밀번호를 DAO의 메소드로 전달해  실행 결과를 리턴받는 메소드.*/
	public int updatePassword(Connection con, String memberId, String password) {
		int result = 0;
		PreparedStatement pstmt = null;
		
		String query = prop.getProperty("updatePassword");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, password);
			pstmt.setString(2, memberId);
			
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		return result;
	}

	/* 성별을 전달해 , 그 성별에 해당하는 회원들의 정보들을 리턴받는 메소드 */
	public List<MemberDTO> selectByGender(Connection con, String gender){
		List<MemberDTO> memberList = null;
		
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("selectByGender");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, gender);
			
			rset = pstmt.executeQuery();
			
			memberList = new ArrayList<>();
			
			while(rset.next()) {
				MemberDTO member = new MemberDTO();
				member.setMemberNo(rset.getInt("MEMBER_NO"));
				member.setMemberId(rset.getString("MEMBER_ID"));
				member.setMemberPwd(rset.getString("MEMBER_PWD"));
				member.setMemberName(rset.getString("MEMBER_NAME"));
				member.setAddress(rset.getString("ADDRESS"));
				member.setGender(rset.getString("GENDER"));
				member.setPhone(rset.getString("PHONE"));
				member.setAge(rset.getInt("AGE"));
				member.setEmail(rset.getString("EMAIL"));
				member.setEnrollDate(rset.getDate("ENROLL_DATE"));
				
				memberList.add(member);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
				
		return memberList;
	}
	/* 변경할 이메일을 전달해 성공여부를 리턴받는 메소드. */
	public int updateEmail(Connection con, String memberId, String email) {
		int result = 0;
		PreparedStatement pstmt = null;
		
		String query = prop.getProperty("updateEmail");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, email);
			pstmt.setString(2, memberId);
			
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		return result;
	}
	/* 특정 사원의 사번과, 그 사원의 변경할 핸드폰 번호를 전달해 실행 결과를 반환받는 메소드 */
	public int updatePhone(Connection con, String memberId, String phone) {
		int result = 0;
		PreparedStatement pstmt = null;
		
		String query = prop.getProperty("updatePhone");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, phone);
			pstmt.setString(2, memberId);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}
	/* 특정 사원의 사번과, 그 사원의 변경할 이메일을 전달해 실행 결과를 반환받는 메소드 */
	public int updateAddress(Connection con, String memberId, String address) {
		int result = 0;
		PreparedStatement pstmt = null;
		
		String query = prop.getProperty("updateAddress");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, address);
			pstmt.setString(2, memberId);
			
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		return result;
	}
	/* 회원번호를 전달해 그 회원을 삭제하는 DAO의 메소드를 호출 후 그 결과를 반환받는다 */
	public int deleteMember(Connection con, String memberId) {
		int result = 0;
		PreparedStatement pstmt = null;
		
		String query = prop.getProperty("deleteMember");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, memberId);
			
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}
}
















