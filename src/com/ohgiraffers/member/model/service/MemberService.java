package com.ohgiraffers.member.model.service;

import static com.ohgiraffers.common.JDBCTemplate.close;
import static com.ohgiraffers.common.JDBCTemplate.commit;
import static com.ohgiraffers.common.JDBCTemplate.getConnection;
import static com.ohgiraffers.common.JDBCTemplate.rollback;

import java.sql.Connection;
import java.util.List;

import com.ohgiraffers.member.model.dao.MemberDAO;
import com.ohgiraffers.member.model.dto.MemberDTO;

public class MemberService {

	private MemberDAO memberDAO = new MemberDAO();
	
	/* 회원가입하기위해 회원정보를 insert하는 dao의 메소드를 호출해서, 
	 * 그 결과가 성공이면 커밋을, 실패하면 rollback을 한다.
	 * 그리고 그 결과를 반환해준다. */
	public int insertMember(MemberDTO member) {
		Connection con = getConnection();
		int result = 0;
		
		result = memberDAO.insertMember(con, member);
		if(result > 0) {
			commit(con);
		} else {
			rollback(con);
		}
		
		close(con);
		return result;
		
	}
	
	/* 모든회원정보를 담아 List<MemberDTO>형태로 리턴해주는 DAO의 메소드를 호출 한 후
	 * 그 결과를 반환한다  */
	public List<MemberDTO> selectAllMembers(){
		Connection con = getConnection();
		List<MemberDTO> memberList = null;
		
		memberList = memberDAO.selectAllMembers(con);
		
		
		close(con);
		return memberList;
	}
	
	/* 조회하고싶은 id를 전달해주면 그 id에 해당하는 회원의 모든 정보를 반환해주는
	 * DAO의 메소드를 호출한다.(id를 전달해서)
	 * 반환받은 해당id의 회원의 정보를 반환해준다 */
	public MemberDTO selectById(String id) {
		Connection con = getConnection();
		MemberDTO member = null;
		
		member = memberDAO.selectById(con, id);
		
		close(con);
		return member;
				
	}
	
	/* DAO에있는 메소드에 특정 성별을 입력해 호출한 후 , 그 해당성별의 회원들의 정보를 반환받는다
	 * 그 후 , 그 정보들을 리턴한다. */
	public List<MemberDTO> selectByGender(String gender){
		Connection con = getConnection();
		List<MemberDTO> result = null;
		
		result = memberDAO.selectByGender(con, gender);
		
		close(con);
		
		return result;
	}
	
	/* DAO에 있는 메소드에 아이디와 변경할 비밀번호를 넣어 호출한다.
	 * 그리고 수행여부를 반환받는데
	 * 성공했으면 commit을 실패했다면 rollback을 한다 */
	public int updatePassword(String memberId, String password) {
		Connection con = getConnection();
		int result = 0;
		
		result = memberDAO.updatePassword(con, memberId, password);
		
		if(result > 0) {
			commit(con);
		} else {
			rollback(con);
		}
		
		close(con);
		
		return result;
	}
	
	/* 특정 회원의 회원번호와 그 회원이 변경할 이메일을 DAO에있는 메소드에 전달해, 호출한다.
	 * 수행 결과에 따라 commit과 rollback을 한 후 , 수행결과를 리턴한다.*/
	public int updateEmail(String memeberId, String email) {
		Connection con = getConnection();
		int result = 0;
		
		result = memberDAO.updateEmail(con, memeberId, email);
		
		if(result > 0) {
			commit(con);
		} else {
			rollback(con);
		}
		close(con);
		
		
		return result;
	}
	
	/* 특정회원의 회원아이디와 변경할 주소를 DAO에 있는 메소드에 전달해 호출한 후
	 * 수행결과를 반환받는다.
	 * 그 수행결과에따라 commit과 rollback을 한 후 
	 * 결과값을 반환한다. */
	public int updateAddress(String memberId, String address) {
		Connection con = getConnection();
		int result = 0;
		
		result = memberDAO.updateAddress(con, memberId, address);
		
		if(result > 0) {
			commit(con);
		} else {
			rollback(con);
		}
		
		close(con);
		
		return result;
	}
	
	/* 특정회원의 아이디와 변경할 핸드폰번호를 DAO에 있는 핸드폰번호를 update해주는 메소드를 호출한 후
	 * 수행결과를 리턴받는다
	 * 수행이 잘 됐으면 commit을 안됐으면 rollback을 한 후 
	 * 전달받았던 수행결과를 리턴해준다, */
	public int updatePhone(String memberId, String phone) {
		Connection con = getConnection();
		int result = 0;
		
		result = memberDAO.updatePhone(con, memberId, phone);
		
		if(result > 0) {
			commit(con);
		} else {
			rollback(con);
		}
		
		return result;
	}
	/* 삭제할 회원의 아이디를 DAO에 있는 메소드에 전달해 호출한다.
	 * 수행결과를 리턴받은 후 , 결과에따라commit과 rollback을 한다.
	 * 리턴받은 결과를 리턴해준다.
	 *  */
	public int deleteMember(String memberId) {
		Connection con = getConnection();
		int result = 0;
		
		result = memberDAO.deleteMember(con, memberId);
		
		if(result > 0) {
			commit(con);
		} else {
			rollback(con);
		}
		
		return result;
	}
	
	
	
}
