 package com.ohgiraffers.member.views;

import java.util.List;

import com.ohgiraffers.member.model.dto.MemberDTO;

public class MemberResultView {
	
	/* DAO에서의 수행결과에 따라 출력될 화면을 담당하는 뷰 들의 모임 */
	public void displayDmlResult(String code) {
		
		switch(code) {
			case "insertFailed" : System.out.println("회원가입 실패 !"); break;
			case "updateFailed" : System.out.println("회원정보 수정 실패 !"); break;
			case "deleteFailed" : System.out.println("회원 탈퇴 실패 !"); break;
			case "selectFailed" : System.out.println("회원 조회 실패 !"); break;
			case "insertSuccess" : System.out.println("insert 성공 !"); break;
			case "updateSuccess" : System.out.println("update 성공 !"); break;
			case "deleteSuccess" : System.out.println("delete 성공 !"); break;
			default : System.out.println("알수없는 에러 발생!"); break;
		}
		
	}
	
	/* 회원정보 여러개를 출력해야될 떄 사용하는 뷰 */
	public void display(List<MemberDTO> memberList) {
		for(MemberDTO member : memberList) {
			System.out.println(member);
		}
	}
	
	/* 한명의 회원정보를 출력해야 될 때 사용하는 뷰 */
	public void display(MemberDTO member) {
		System.out.println(member);
	}
	
}
