package com.ohgiraffers.member.views;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import com.ohgiraffers.member.controller.MemberController;

public class MemberMenu {

	private Scanner sc = new Scanner(System.in);
	
	/* 프로그램의 메인 화면을 출력해주는 뷰 */
	public void displayMenu() {
		MemberController memberController = new MemberController();
		
		do {
			System.out.println();
			System.out.println();
			System.out.println(" *** 회원 관리 프로그램 *** ");
			System.out.println("1. 새 회원 등록");
			System.out.println("2. 모든 회원 조회");
			System.out.println("3. 아이디로 회원 조회");
			System.out.println("4. 성별로 회원 조회");
			System.out.println("5. 암호 수정");
			System.out.println("6. 이메일 변경");
			System.out.println("7. 전화번호 변경");
			System.out.println("8. 주소 변경");
			System.out.println("9. 회원 탈퇴");
			System.out.print("번호 선택 : ");
			int no = sc.nextInt();
			sc.nextLine();
			switch(no) {
				case 1: memberController.registNewMember(inputMember()); break;
				case 2: memberController.selectAllMembers(); break;
				case 3: memberController.searchMemberById(inputMemberId()); break;
				case 4: memberController.searchMemberByGender(inputGender()); break;
				case 5: memberController.modifyPassword(inputMemberId(), inputPassword()); break;
				case 6: memberController.modifyEmail(inputMemberId(), inputEmail()); break;
				case 7: memberController.modifyPhone(inputMemberId(), inputPhone()); break;
				case 8: memberController.modifyAddress(inputMemberId(), inputAddress()); break;
				case 9: memberController.deleteMember(inputMemberId()); break;
				case 0:return;
				default : System.out.println("잘못된 번호입니다. 다시입력해주세요.");
			}
			
		}while(true);
		
	}
	
	/* 회원의 아이디를 입력받을 떄 사용하는 뷰 */
	public String inputMemberId() {
		System.out.print("회원 아이디 : ");
		return sc.nextLine();			//id를 입력받아 리턴한다				
	}
	
	/* 회원의 모든 정보를 입력받을때 사용하는 뷰 */
	public Map<String, String> inputMember(){
		
		Map<String, String> personInfo = new HashMap<>();
		
		System.out.print("입력할 아이디 : ");
		personInfo.put("id", sc.nextLine());
		System.out.print("입력할 비밀번호 : ");
		personInfo.put("pwd", sc.nextLine());
		System.out.print("입력할 이름 : ");
		personInfo.put("name", sc.nextLine());
		System.out.print("입력할 성별(남:M/여:F) : ");
		personInfo.put("gender", sc.nextLine().toUpperCase());
		System.out.print("입력할 이메일 : ");
		personInfo.put("email", sc.nextLine());
		System.out.print("입력할 전화번호 : ");
		personInfo.put("phone", sc.nextLine());
		System.out.print("입력할 주소 : ");
		personInfo.put("address", sc.nextLine());
		System.out.print("입력할 나이 : ");
		personInfo.put("age", sc.nextLine());
		
		return personInfo;
	}
	
	/* 회원의 성별을 입력받을때 사용하는 뷰 */
	public String inputGender() {
		System.out.print("조회할 성별 입력(남:M/여:F) : ");
		return sc.nextLine().toUpperCase();
	}
	
	/* 회원의 비밀번호를 입력받을 때 사용하는 뷰 */
	public String inputPassword() {
		System.out.print("수정할 비밀번호 입력 : ");
		return sc.nextLine();
	}
	
	/* 회원의 이메일을 입력받을 때 사용하는 뷰*/
	public String inputEmail() {
		System.out.print("수정할 이메일 입력 : ");
		return sc.nextLine();
	}
	
	/* 회원의 전화번호를 입력받을 때 사용하는 뷰*/
	public String inputPhone() {
		System.out.print("수정할 전화번호 입력 : ");
		return sc.nextLine();
	}
	
	/* 회원의 주소를 입력받을 때 사용하는 뷰*/
	public String inputAddress() {
		System.out.print("수정할 주소 입력 : ");
		return sc.nextLine();
	}
}



































